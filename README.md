# Fakedatagen

A small cli data generator for one of my projects. It retrieves data from external websites, parse it to a model and 
generates an output file based on a template.


### Installation

I have no plan to publish it to PyPi for now (after all, the program is not extraordinary), so you'll have to build 
manually until I configure a build pipeline.

Clone the current repository, then run `pip3 install -e .`

Alternatively, you can run `pip3 install ".[all]"` to install the project and the extra tools

Now, your environment (or venv) should contain the `fakedatagen` command.

### Usage

Run the program from command line with the following synopsys:

```
usage: fakedatagen [-h] [-c ENTRIES_COUNT] -t TEMPLATE_FILENAME -o OUTPUT_FILENAME
```

| Argument | Required | Description |
|----------|----------|-------------|
| `-h`, `--help` | no | Prints the help menu |
| `-c`, `--entries_count`| no (default 50) | The number of data entries to generate |
| `-t`, `--template_filename`| yes | The path to the template file to use |
| `-o`, `--output_filename`| yes | The path to the output file to generate |

### Templating and model

The templates files are Jinja2 templates, and they have the following `model̀` object available to them:
```
{
    addresses: [
        {
            street: ... ,
            zipCode: ... ,
            city: ... ,
            region: ... ,
            country: ... ,
            additional: ...
        },
        ...
    ]
    individuals: [
        {
            firstname: ... ,
            lastname: ... ,
            email_address: ...
        },
        ...
    ]
}
```

You can have a look at the [samples/templates](samples/templates/) directory to see what could be done with the generator.

### License

This code is released under [GPLv3](LICENSE.md).
