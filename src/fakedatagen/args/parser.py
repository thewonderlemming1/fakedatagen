from argparse import ArgumentParser
from fakedatagen.args.options import Options
from fakedatagen.args.types import EntriesCountType, TemplateFilename, OutputFilename
from typing import Optional


class CmdParser:

    def __init__(self):
        self._parser = ArgumentParser(prog='fakedatagen', description='Retrieves fake data from the Internet and '
                                                                      'generates a datafile given a template.')
        self._build_args()

    def parse(self, args) -> Optional[Options]:
        """Parses the given command line args and returns an Options container for the supported options.

        :param args: the command line arguments.
        :return: an Options object.
        """
        parsed = self._parser.parse_args(args)
        return Options(entries_count=parsed.entries_count, template_filename=parsed.template_filename,
                       output_filename=parsed.output_filename)

    def _build_args(self):
        """Builds the command line parser arguments.
        """
        self._parser.add_argument('-c', '--entries_count',
                                  type=EntriesCountType.get,
                                  default='50',
                                  required=False,
                                  help='the number of entries to generate')
        self._parser.add_argument('-t', '--template_filename',
                                  type=TemplateFilename.get,
                                  required=True,
                                  help='the path to the template file to use')
        self._parser.add_argument('-o', '--output_filename',
                                  type=OutputFilename.get,
                                  required=True,
                                  help='the path to the output file to generate')
