from argparse import ArgumentTypeError
from os.path import exists, isfile
from re import match


class EntriesCountType:

    _INTEGER_REGEX = '^(-)?[0-9]+$'

    @staticmethod
    def get(entries_count: str):
        """Takes the given input and ensures that it meets the expectations.

        :param entries_count: the argument to validate.
        :raises ArgumentTypeError: if the expectations are not met.
        :return: the given value as is.
        """
        if not match(EntriesCountType._INTEGER_REGEX, entries_count):
            raise ArgumentTypeError(f'Expected entries count to be an integer but got {type(entries_count)} instead')

        entries_count = int(entries_count)
        if entries_count <= 0:
            raise ArgumentTypeError(f'Expected entries count to be a positive number but got {entries_count} instead')
        return entries_count


class TemplateFilename:

    @staticmethod
    def get(template_filename: str):
        """Takes the given input and ensures that it meets the expectations.

        :param template_filename: the argument to validate.
        :raises ArgumentTypeError: if the expectations are not met.
        :return: the given value as is.
        """
        if not exists(template_filename):
            raise ArgumentTypeError(f'Could not find template file {template_filename}')
        elif not isfile(template_filename):
            raise ArgumentTypeError(f'Template "{template_filename}" is not a file')
        return template_filename


class OutputFilename:

    @staticmethod
    def get(output_filename: str):
        """Takes the given input and ensures that it meets the expectations.

        :param output_filename: the argument to validate.
        :raises ArgumentTypeError: if the expectations are not met.
        :return: the given value as is.
        """
        if exists(output_filename):
            raise ArgumentTypeError(f'Output file "{output_filename}" already exists.')
        return output_filename
