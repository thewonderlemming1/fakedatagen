from dataclasses import dataclass


@dataclass
class Options:

    entries_count: int
    template_filename: str
    output_filename: str
