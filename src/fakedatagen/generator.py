from fakedatagen.args.parser import CmdParser
from fakedatagen.datasource.addresses import AddressesDatasource
from fakedatagen.datasource.individuals import IndividualsDatasource
from fakedatagen.rendering.render import Renderer


def main(args):
    """Parses the given command line options, retrieves/generates the data, and renders the given template to an output
    file.

    :param args: the command line args.
    """
    options = CmdParser().parse(args)
    model = {
        'addresses': AddressesDatasource(options).get(),
        'individuals': IndividualsDatasource(options).get()
    }
    Renderer(options).render(model)
