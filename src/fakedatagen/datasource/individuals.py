from fakedatagen.args.options import Options
from fakedatagen.datasource.datasource import AbstractDatasource
from fakedatagen.model.individual import Individual
from random import choice, randint


class IndividualsDatasource(AbstractDatasource):

    _ALLOWED_CHARACTERS = 'abcdefghijklmnopqrstuvwxyz'
    _MIN_FIRSTNAME_LEN = 3
    _MAX_FIRSTNAME_LEN = 7
    _MIN_LASTNAME_LEN = 2
    _MAX_LASTNAME_LEN = 10

    def __init__(self, options: Options):
        self._individuals_count = options.entries_count

    def get(self) -> list[Individual]:
        """Generates random firstname/lastname couples and creates a list of Individual objects.

        :return: a list of Individual objects.
        """
        return [Individual.build_from_name_couple(self._get_firstname(), self._get_lastname())
                for i in range(0, self._individuals_count)]

    def _get_firstname(self) -> str:
        """Generates a random firstname of random size and returns it.

        :return: the generated firstname.
        """
        firstname_size = randint(self._MIN_FIRSTNAME_LEN, self._MAX_FIRSTNAME_LEN)
        return ''.join(choice(self._ALLOWED_CHARACTERS) for i in range(firstname_size))

    def _get_lastname(self) -> str:
        """Generates a random lastname of random size and returns it.

        :return: the generated lastname.
        """
        lastname_size = randint(self._MIN_LASTNAME_LEN, self._MAX_LASTNAME_LEN)
        return ''.join(choice(self._ALLOWED_CHARACTERS) for i in range(lastname_size))
