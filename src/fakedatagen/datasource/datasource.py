from abc import ABC, abstractmethod
from typing import Any


class AbstractDatasource(ABC):

    @abstractmethod
    def get(self) -> list[Any]:
        pass
