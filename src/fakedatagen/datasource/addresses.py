import http.client
from fakedatagen.args.options import Options
from fakedatagen.datasource.datasource import AbstractDatasource
from codecs import encode
from math import ceil
from fakedatagen.model.address import Address


class AddressesDatasource(AbstractDatasource):

    _MAX_COUNT = 50
    _COUNTRY = 'fr'

    def __init__(self, options: Options):
        self._addresses_count = options.entries_count

    def get(self) -> list[Address]:
        """Fetches the expected number of addresses from the Internet, parses it and returns a model.

        :return: a list of Address objects.
        """
        addresses: list[Address] = []
        max_count = ceil(self._addresses_count / self._MAX_COUNT)
        for i in range(0, max_count):
            if self._addresses_count <= self._MAX_COUNT:
                count = self._addresses_count
            elif i == max_count - 1:
                count = self._addresses_count - (i * self._MAX_COUNT)
            else:
                count = self._MAX_COUNT
            addresses += Address.build_from_json_array(self._retrieve_from_source(count))

        print(f'Fetched {len(addresses)} addresses: {addresses}')
        return addresses

    @staticmethod
    def _retrieve_from_source(count):
        """Fetches a given number of addresses from the Internet.

        :param count: the number of addresses to fetch.
        :return: the results of the HTTP call.
        """
        connection = http.client.HTTPSConnection("randommer.io")
        boundary = 'wL36Yn8afVp8Ag7AmP8qZ0SA4n1v9T'
        payload = b'\r\n'.join([
            encode('--' + boundary),
            encode('Content-Disposition: form-data; name=number;'),
            encode('Content-Type: {}'.format('text/plain')),
            encode(''),
            encode(f"{count}"),
            encode('--' + boundary),
            encode('Content-Disposition: form-data; name=Culture;'),
            encode('Content-Type: {}'.format('text/plain')),
            encode(''),
            encode("fr"),
            encode('--' + boundary + '--'),
            encode('')
        ])
        headers = {'Content-type': 'multipart/form-data; boundary={}'.format(boundary)}
        connection.request("POST", "/random-address", payload, headers)
        res = connection.getresponse()
        data = res.read()

        results = data.decode("utf-8").replace('["', '').replace('"]', '').split('","')
        return results
