from dataclasses import dataclass


@dataclass
class Individual:

    firstname: str
    lastname: str
    email_address: str

    @staticmethod
    def build_from_name_couple(firstname: str, lastname: str):
        """Creates an email address based on the provided firstname/lastname couple, then returns a new instance of
        Individual.

        :param firstname: the firstname of the individual to create.
        :param lastname: the lastname of the individual to create.
        :return: the newly created Individual instance.
        """
        email_address = f'{firstname}.{lastname}@azqore.ch.pbk'
        return Individual(firstname, lastname, email_address)
