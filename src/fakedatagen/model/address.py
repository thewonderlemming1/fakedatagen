from dataclasses import dataclass


@dataclass
class Address:

    street: str
    zipCode: str
    city: str
    region: str
    country: str
    additional: str = ''

    @staticmethod
    def build_from_json_array(json_payload: list[str]):
        """Parses the JSON result provided by the Datasource and returns a list of addresses.

        :param json_payload: the datasource JSON result.
        :return: a list of Address objects.
        """
        addresses: list[Address] = []
        for item in json_payload:
            columns = [c.strip() for c in item.split(',') if c]
            addresses.append(Address(street=columns[0],
                                     additional=columns[1],
                                     zipCode=columns[2],
                                     city=columns[3],
                                     region=columns[4],
                                     country=columns[5]))

        return addresses
