from args.options import Options
from jinja2 import Environment, FileSystemLoader
from os import makedirs
from os.path import basename
from pathlib import Path


class Renderer:

    def __init__(self, options: Options):
        self._template = basename(options.template_filename)
        self._output = options.output_filename
        self._env = self._create_environment(options)
        self._create_output_directories()

    def render(self, model: dict):
        """Takes the given model and assigns it to the given template file, then render it and writes the output to the
        output file.

        :param model: the model to render
        """
        template = self._env.get_template(self._template)
        results = template.render(model=model)
        with open(self._output, 'w') as f:
            f.write(results)

    def _create_output_directories(self):
        """Creates the intermediate parent directories for the given output file.
        """
        output_dir = f'{Path(self._output).parent.absolute()}'
        makedirs(output_dir, exist_ok=True, mode=0o755)

    @staticmethod
    def _create_environment(options: Options) -> Environment:
        """Creates the Jinja2 environment to load the template from.

        :param options:
        :return: the Jinja2 environment to use.
        """
        tpl_dir = f'{Path(options.template_filename).parent.absolute()}'
        return Environment(loader=FileSystemLoader(tpl_dir), autoescape=True, trim_blocks=True)
